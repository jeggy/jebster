<?php
include 'design/header.php';
require 'connect.php';
echo "<title>$sitetitle | Create a new album</title>";



if(logged_in()){

if(isset($_POST['album_name'], $_POST['album_description'])){
	$album_name = $_POST['album_name'];
	$album_description = $_POST['album_description'];
	
	$errors = array();
	
	if(empty($album_name) || empty($album_description)){
		$errors[] = 'Album name and description required';
	}else{
		if(strlen($album_name) > 55 || strlen($album_description) >255){
			$errors[] = "One or more fields contains to many characters.";
		}
	}
	
	if(!empty($errors)){
		foreach($errors as $error){
			echo $error."<br />";
		}
	}else{
		create_album($album_name, $album_description);
		header("Location: myalbums.php");
	}
}
?>
<form method='POST'>
	<p>Name:<br /><input type='text' name='album_name' maxlength='55' /></p>
	<p>Description:<br /><textarea name='album_description' rows='6' cols='35' maxlength='255'></textarea></p>
	<p><input type='submit' value='Create'></p>
</form>
<?php

}else{
	echo "You need to be logged in to view this page.";
}

include 'design/footer.php';
?>