<?php
$sitetitle = "Jebster";
$textface = 'Arial';
$needlogin = "<center>You need to be logged in <br /> Register <a href='register.php' id='anone'>here</a></center>";


include 'jqdata.php';
include 'func/image.func.php';
include 'func/thumb.func.php';
include 'func/user.func.php';
include 'func/album.func.php';

?>

<link rel='StyleSheet' href='style.css' type='text/css'>
<script type="text/javascript" src="js/clock.js"></script>
<!--[if IE]><link rel="stylesheet" href="ie.css" type="text/css" media="screen, projection" /><![endif]-->
<?php
if(isset($_SESSION['id'])){$id=$_SESSION['id'];}else{$id=NULL;}
Class resize
{
/*	
Include the class
include("resize-class.php");
1) Initialise / load image
$resizeObj = new resize('test.jpg');
Resize image (options: exact, portrait, landscape, auto, crop)
$resizeObj -> resizeImage(160, 180, 'crop');
3) Save image
$resizeObj -> saveImage('test-resized.jpg', 100);
*/
	private $image;
    private $width;
    private $height;
	private $imageResized;
	function __construct($fileName)
	{
		$this->image = $this->openImage($fileName);
	    $this->width  = imagesx($this->image);
	    $this->height = imagesy($this->image);
	}
	private function openImage($file)
	{
		$extension = strtolower(strrchr($file, '.'));
			switch($extension)
		{
			case '.jpg':
			case '.jpeg':
				$img = @imagecreatefromjpeg($file);
				break;
			case '.gif':
				$img = @imagecreatefromgif($file);
				break;
			case '.png':
				$img = @imagecreatefrompng($file);
				break;
			default:
				$img = false;
				break;
		}
		return $img;
	}
	public function resizeImage($newWidth, $newHeight, $option="auto")
	{
		$optionArray = $this->getDimensions($newWidth, $newHeight, $option);
		$optimalWidth  = $optionArray['optimalWidth'];
		$optimalHeight = $optionArray['optimalHeight'];
		$this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
		imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);
		if ($option == 'crop') {
			$this->crop($optimalWidth, $optimalHeight, $newWidth, $newHeight);
		}
	}
	private function getDimensions($newWidth, $newHeight, $option)
	{
		   switch ($option)
		{
			case 'exact':
				$optimalWidth = $newWidth;
				$optimalHeight= $newHeight;
				break;
			case 'portrait':
				$optimalWidth = $this->getSizeByFixedHeight($newHeight);
				$optimalHeight= $newHeight;
				break;
			case 'landscape':
				$optimalWidth = $newWidth;
				$optimalHeight= $this->getSizeByFixedWidth($newWidth);
				break;
			case 'auto':
				$optionArray = $this->getSizeByAuto($newWidth, $newHeight);
				$optimalWidth = $optionArray['optimalWidth'];
				$optimalHeight = $optionArray['optimalHeight'];
				break;
			case 'crop':
				$optionArray = $this->getOptimalCrop($newWidth, $newHeight);
				$optimalWidth = $optionArray['optimalWidth'];
				$optimalHeight = $optionArray['optimalHeight'];
				break;
		}
		return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
	}
	private function getSizeByFixedHeight($newHeight)
	{
		$ratio = $this->width / $this->height;
		$newWidth = $newHeight * $ratio;
		return $newWidth;
	}
	private function getSizeByFixedWidth($newWidth)
	{
		$ratio = $this->height / $this->width;
		$newHeight = $newWidth * $ratio;
		return $newHeight;
	}
	private function getSizeByAuto($newWidth, $newHeight)
	{
		if ($this->height < $this->width)
		{
			$optimalWidth = $newWidth;
			$optimalHeight= $this->getSizeByFixedWidth($newWidth);
		}
		elseif ($this->height > $this->width)
		{
			$optimalWidth = $this->getSizeByFixedHeight($newHeight);
			$optimalHeight= $newHeight;
		}
		else
		{
			if ($newHeight < $newWidth) {
				$optimalWidth = $newWidth;
				$optimalHeight= $this->getSizeByFixedWidth($newWidth);
			} else if ($newHeight > $newWidth) {
				$optimalWidth = $this->getSizeByFixedHeight($newHeight);
				$optimalHeight= $newHeight;
			} else {
				$optimalWidth = $newWidth;
				$optimalHeight= $newHeight;
			}
		}
		return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
	}
	private function getOptimalCrop($newWidth, $newHeight)
	{
		$heightRatio = $this->height / $newHeight;
		$widthRatio  = $this->width /  $newWidth;
		if ($heightRatio < $widthRatio) {
			$optimalRatio = $heightRatio;
		} else {
			$optimalRatio = $widthRatio;
		}
		$optimalHeight = $this->height / $optimalRatio;
		$optimalWidth  = $this->width  / $optimalRatio;
		return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
	}
	private function crop($optimalWidth, $optimalHeight, $newWidth, $newHeight)
	{
		$cropStartX = ( $optimalWidth / 2) - ( $newWidth /2 );
		$cropStartY = ( $optimalHeight/ 2) - ( $newHeight/2 );
		$crop = $this->imageResized;
		$this->imageResized = imagecreatetruecolor($newWidth , $newHeight);
		imagecopyresampled($this->imageResized, $crop , 0, 0, $cropStartX, $cropStartY, $newWidth, $newHeight , $newWidth, $newHeight);
	}
	public function saveImage($savePath, $imageQuality="100")
	{
      		$extension = strrchr($savePath, '.');
     			$extension = strtolower($extension);
		switch($extension)
		{
			case '.jpg':
			case '.jpeg':
				if (imagetypes() & IMG_JPG) {
					imagejpeg($this->imageResized, $savePath, $imageQuality);
				}
				break;
			case '.gif':
				if (imagetypes() & IMG_GIF) {
					imagegif($this->imageResized, $savePath);
				}
				break;
			case '.png':
				$scaleQuality = round(($imageQuality/100) * 9);
				$invertScaleQuality = 9 - $scaleQuality;
				if (imagetypes() & IMG_PNG) {
					 imagepng($this->imageResized, $savePath, $invertScaleQuality);
				}
				break;
			default:
				break;
		}
		imagedestroy($this->imageResized);
	}
}

function buttonLinks($rpp,$dbtable,$tb_el=NULL,$tbr=NULL){
/*
arg0: Rows per page.
arg1: Name of db table.
optional.
arg2: What table row equals to.
arg3: Name of table row.
*/
$rowsPerPage = $rpp;

$pageNum = 1;

if(isset($_GET['page']))
{
	$pageNum = $_GET['page'];
}
$offset = ($pageNum - 1) * $rowsPerPage;
if($tb_el==NULL){
	
	$query = "SELECT id FROM $dbtable LIMIT $offset, $rowsPerPage";
}else{
	$query = "SELECT id FROM $dbtable WHERE $tbr='$tb_el' LIMIT $offset, $rowsPerPage";
}
$result = mysql_query($query) or die('Error, query failed');

while($row = mysql_fetch_array($result))
{
	echo $row['id'] . '<br>';
}
if($tb_el==NULL){
	$query = "SELECT COUNT(id) AS numrows FROM $dbtable";
}else{
	$query = "SELECT COUNT(id) AS numrows FROM $dbtable WHERE $tbr='$tb_el'";
}
$result  = mysql_query($query) or die('Error, query failed');
$row     = mysql_fetch_array($result, MYSQL_ASSOC);
$numrows = $row['numrows'];

$maxPage = ceil($numrows/$rowsPerPage);
$self = $_SERVER['PHP_SELF'];
$nav  = '';

for($page = 1; $page <= $maxPage; $page++)
{
	if ($page == $pageNum)
	{
		$nav .= "[$page]";
	}
	else
	{
		$nav .= " <a href=\"$self?page=$page\">".floor($page)."</a> ";
	}
}
if ($pageNum > 1)
{
	$page  = $pageNum - 1;
	$prev  = " <a href=\"$self?page=$page\">[Prev]</a> ";
	$first = " <a href=\"$self?page=1\">[First Page]</a> ";
}
else
{
	$prev  = '&nbsp;';
	$first = '&nbsp;'; 
}
if ($pageNum < $maxPage)
{
	$page = $pageNum + 1;
	$next = " <a href=\"$self?page=$page\">[Next]</a> ";
	$last = " <a href=\"$self?page=$maxPage\">[Last Page]</a> ";
}
else
{
	$next = '&nbsp;';
	$last = '&nbsp;'; 
}

echo $first . $prev . $nav . $next . $last;
$nav  = '';
for($page = 1; $page <= $maxPage; $page++)
{
	if ($page == $pageNum)
	{
		$nav .= " [ $page ] ";
	}
	else
	{
	$nav .= " <a href=\"$self?page=$page\">".floor($page)."</a> ";
	}
}
}



function fadeInOut() {


$x = "jogvan";
?>

<style type='text/css'>
.button {
font-family: Georgia;
border: 0px;
padding:0px;
cursor:pointer;
font-size: 1em;
background-color: white;
vertical-align:bottom;
color: #33cc33;
text-decoration: none;
}
.button:hover {
font-family: Georgia;
border:0px;
padding:0px;
cursor:pointer;
background-color: white;
color: #33ff00;
text-decoration: underline;
}
</style>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.js'></script>
<script type='text/javascript'>
	function get() {
		$('#lastname').hide();
		$.post('jqdata.php', { firstname: '<?php echo $x; ?>' }, 
			function(output){
				$('#lastname').html(output).fadeIn(1000);
			}
		);
	}
	

</script>

</p>
<form name='form' method='POST'>
	<input type='button' onClick='get();' class='button' value='Get'>
</form>
<div id='lastname'></div>
</p>

<?php
}



function lastAction($utime,$udate){
/*
arg1 = time
arg2 = date
*/
	$datenow = date("j/m-y");
	if($udate==$datenow)
	{
		$times = explode(":", $utime);
		$x = 0;
		foreach ($times as $time): 
			if($x==0){
				$t1 = $time;
			}elseif($x==1){
				$t2 = $time;
			}
			elseif($x==2){
				$t3 = $time;
			}
			$x++;
		endforeach;
		$ntime = date('H')*60*60+date('i')*60+date('s');
		$latime = $t1*60*60+$t2*60+$t3;
		$latime;
		$newtime = $ntime-$latime;
		$newtime = number_format($newtime, 0, '.', '');
		$newtimem = 0;
		$newtimeh = 0;
		while($newtime>=60)
		{
			$newtime = $newtime-60;
			$newtimem++;
		}
		while($newtimem>=60)
		{
			$newtimem = $newtimem-60;
			$newtimeh++;
		}
		if($newtimeh!=0){
			if($newtimeh==1)
			{
				echo $newtimeh." Hour ago";
			}else{
				echo $newtimeh." Hours ago";
			}
		}elseif($newtimem!=0){
			if($newtimem==1)
			{
				echo $newtimem." Minute ago";
			}else{
				echo $newtimem." Minutes ago";
			}
		}else{
			if($newtime==1)
			{
				echo $newtime." Second ago";
			}else{
				echo $newtime." Seconds ago";
			}
		}
	}
	else{
		Timeago($udate);
	}
}

if(isset($_GET['clock'])){$clock = $_GET['clock'];}else{$clock="false";}
if($clock=="true"){
	echo date("H:i:s")."<br>";
	echo date("j M Y")."<p>";
}
function curPageURL() {
	if(isset($_SERVER['SERVER_PORT'])&&isset($_SERVER['SERVER_NAME'])&&isset($_SERVER['REQUEST_URI'])){
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL = $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL = $_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}else{
		echo "none";
	}
}

function ucfyrst($string) {
    $sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
    $new_string = '';
    foreach ($sentences as $key => $sentence) {
        $new_string .= ($key & 1) == 0?
            ucfirst(strtolower(trim($sentence))) :
            $sentence.' ';
    }
    return trim($new_string);
}

function getExtension($str) {
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}


function getAge( $p_strDate ) {
	list($Y,$m,$d)	= explode("-",$p_strDate);
	$years			= date("Y") - $Y;
	if( date("md") < $m.$d ) { $years--; }
	return $years;
}



function FBBcode($texto){
   $c = array(
	  "/\[i\](.*?)\[\/i\]/is", // nr.1
      "/\[b\](.*?)\[\/b\]/is", // nr.2
      "/\[u\](.*?)\[\/u\]/is", // nr.3
      "/\[img\](.*?)\[\/img\]/is", // nr.4
      "/\[url=(.*?)\](.*?)\[\/url\]/is", //nr.5
	  "/\[quote\](.*?)\[\/quote\]/is", // nr.6
	  "/\[color=(.*?)\](.*?)\[\/color\]/is", //nr.7
	  "/\[ytb\](.*?)\[\/ytb\]/is", // nr.8
	  "/\[youtube\](.*?)\[\/youtube\]/is", // nr.9
	  "/\[size=(.*?)\](.*?)\[\/size\]/is", //nr.10
	  // Smileys
	  "/\:D/is", //nr.S1
	  "/\:P/is", //nr.S2
	  "/\:XP/is", // nr.S3
	  "/\:Q/is", // nr.S4
	  "/\:\'\(/s", // nr.S5
	  "/\>:\(/s", // nr.S6 
	  "/\:\|/s", // nr.S7
	  "/\:\(/s", // nr.S8
	  "/\:O/s", // nr.S9
	  "/\:\)/s", // nr.S10
	  
   );
   $d = array(
	  "<i>$1</i>", //nr.1
      "<b>$1</b>", //nr.2
      "<u>$1</u>", //nr.3
      "<img src=\"$1\" / style='max-width: 300px;'/>", //nr.4
      "<a href=\"$1\" id='anone' target=\"_blank\">$2</a>", //nr.5
	  "<blockquote style='border: 2px solid #33cc00; background-color: #EAF2D3;' >$1</blockquote>",
	  //"<p style='border: 2px solid #33cc00; background-color: #EAF2D3; '>$1</p>", //nr.6
	  //"<table cellspacing='0' border='1' bordercolor='#33cc00'><tr style='background-color: #EAF2D3;'><td>$1</td></tr></table>", //nr.6
	  "<font color='$1'>$2</font>", // nr.7
	  "<iframe width='318' height='261' src='http://www.youtube.com/embed/$1' frameborder='0' allowfullscreen></iframe>", // nr.8
	  "<iframe width='318' height='261' src='http://www.youtube.com/embed/$1' frameborder='0' allowfullscreen></iframe>", // nr.9
	  "<font size='$1'>$2</font>", // nr.10
	  // Smileys
	  "<img src='smiley/forum/biggrin.gif'>", //nr.S1
	  "<img src='smiley/forum/tongue.gif'>", // nr.S2
	  "<img src='smiley/forum/bleh.gif'>",  // nr.S3
	  "<img src='smiley/forum/what.gif'>", // nr.S4
	  "<img src='smiley/forum/cry.gif'>", // nr.S5
	  "<img src='smiley/forum/mad.gif'>", // nr.S6
	  "<img src='smiley/forum/mellow.gif'>", // nr.S7
	  "<img src='smiley/forum/sad.gif'>", // nr.S8
	  "<img src='smiley/forum/jawdrop.gif'>", // nr.S9
	  "<img src='smiley/forum/happy.gif'>", // nr.S10
   );
   $texto = preg_replace($c, $d, $texto);
   $texto = nl2br($texto);
   return $texto;
}



function BBcode($texto){
   $a = array(
      "/\[i\](.*?)\[\/i\]/is", // nr.1
	"/\[b\](.*?)\[\/b\]/is", // nr.2
	"/\[u\](.*?)\[\/u\]/is", // nr.3
	"/\[img\](.*?)\[\/img\]/is", // nr.4
	"/\[url=(.*?)\](.*?)\[\/url\]/is", //nr.5
	"/\[size=(.*?)\](.*?)\[\/size\]/is", //nr.6
	"/\[color=(.*?)\](.*?)\[\/color\]/is", //nr.7
	"/\[ytb\](.*?)\[\/ytb\]/is", // nr.8
	"/\[youtube\](.*?)\[\/youtube\]/is", // nr.9
	// Smileys
	"/\:D/is", //nr.S1
	"/\:P/is", //nr.S2
	"/\:XP/is", // nr.S3
	"/\:Q/is", // nr.S4
   );
   $b = array(
	"<i>$1</i>", //nr.1
	"<b>$1</b>", //nr.2
	"<u>$1</u>", //nr.3
	"<img src=\"$1\" / style='max-width: 300px;'>", //nr.4
	"<a href=\"$1\" id='anone' target=\"_blank\">$2</a>", //nr.5
	"<font size=$1>$2</font>", //nr.6
	"<font color=$1>$2</font>", //nr.7
	"<object width='425' height='350'><param name='movie' value='http://www.youtube.com/v/$1' /><param name='wmode' value='transparent' /><embed src='http://www.youtube.com/v/$1'></object>", // nr.8
	"<object width='425' height='350'><param name='movie' value='http://www.youtube.com/v/$1' /><param name='wmode' value='transparent' /><embed src='http://www.youtube.com/v/$1'></object>", // nr.10
	// Smileys
	"<img src='smiley/forum/biggrin.gif'>", //nr.S1
	"<img src='smiley/forum/tongue.gif'>", // nr.S2
	"<img src='smiley/forum/bleh.gif'>",  // nr.S3
	"<img src='smiley/forum/what.gif'>", // nr.S4
   );
   $texto = preg_replace($a, $b, $texto);
   $texto = nl2br($texto);
   return $texto;
}





function visitorsNow()
{
$session_path = session_save_path();
$visitors=0;
$handle = opendir($session_path);

while(($file = readdir($handle))!= FALSE)
{
	if($file!="."&&$file!="..")
	{
		if(ereg("^sess",$file))
		{
			$visitors++;
		}
	}
}
echo "There are currently $visitors visitors online";
}

function protect($v)
{
	$v = trim($v);
	$v = strip_tags($v);
	return $v;
}



function DateToDate($date){
$explode1 = explode("/", $date); 
	$x1 = 0;
	foreach($explode1 as $ex1):
		if($x1==0){
			$ex1a = $ex1;
		}elseif($x1==1){
			$explode2 = explode("-",$ex1);
			$x2=0;
			foreach($explode2 as $ex2):
				if($x2==0){
					$ex2a = $ex2;
				}elseif($x2==1){
					$ex2b = $ex2;
				}
				$x2++;
			endforeach;
		}
		$x1++;
	endforeach;
	
	$all = "20".$ex2b."-";
	$all .= $ex2a."-";
	$all .= $ex1a;
	
	return $all;
}

function TimeAgo($date){
	
	$dateandtime = DateToDate($date);
	
	if(isset($row['time'])){
		$dateandtime .= " ".$row['time'];
	}
	
	$now = date("Y-m-d H:i:s");
	$today = strtotime($now);
	$myBirthDate = strtotime($dateandtime);
	$dago = round(abs($today-$myBirthDate)/60/60/24);
	if($dago==1){
		echo $dago." Day ago";
	}else{
		echo $dago." Days ago";
	}
	
}




function comUrl() {
	$curpage = curPageURL();
	$npage = str_replace("&","*",$curpage);
	return $npage;
}




function user_inf($id){
	$query = mysql_query("SELECT * FROM users WHERE id='$id'");
	return mysql_fetch_assoc($query);
	
}



















































/*
resize and crop (crop format 4:3 or 1:1)
- methode resample/resize
- open jpg, gif, bmp, png
- save jpg (85%), gif, bmp, png
- show images
*/

$FILE_MIMES = array(
'image/pjpeg'=>"jpg",
'image/jpeg'=>"jpg",
'image/jpg'=>"jpg",
'image/png'=>"png",
'image/x-png'=>"png",
'image/gif'=>"gif",
'image/bmp'=>"bmp");
$FILE_EXTS  = array('.jpeg','.jpg','.png','.gif','.bmp');
$MAX_SIZE  = 16777216;

$lng=array(
's_imgDimension'=>array(
       "80",
       "320"
       ),
's_imgCrop'    =>array(
       "- auto -",
       "na sirku",
       "na vysku",
       "ctverec",
       "- zadne -"
       ),
's_imgFormat'    =>array(
       "4/3",
       "1/1"
       ),
's_imgType'    =>array(
       "jpg",
       "png",
       "gif",
       "bmp"
       ),
's_imgMethode'    =>array(
       "resample",
       "resize"
       )
);

function PPimageTransform($photo1,$type1,$dim,$crop,$format,$methode,$photo2,$type2)
{
if ($photo1!=="" && File_Exists($photo1))
{
   if (!($crop>0 && $crop<5)) $crop=0;
   $width="x";
   list($w,$h) = GetImageSize($photo1);
   $w=($w<1)?1:$w;
   $h=($h<1)?1:$h;
   $wh=$w/$h;

   // format
   $format=($format=='4/3')?4/3:1;

   // auto crop
   if ($crop==0)
       {
       $z1=$format-  ($format-1)/2;
       $z2=$format+(1-$format-1)/2;
           if ($wh>$z1) {$crop=1;}
       elseif ($wh<$z2) {$crop=2;}
       else        {$crop=3;}
       }

   // scale
   if ($crop==4)
       {
       $a=$w>$h?$w:$h;
       $scale=$dim/$a;
       $nw=$w*$scale;$nw=($nw<1)?1:$nw;
       $nh=$h*$scale;$nh=($nh<1)?1:$nh;

       $d=array(0,0,floor($nw),floor($nh));
       $s=array(0,0,$w,$h);
       }
   else
       {
           if ($crop==3) {$nw=1;$nh=1;}
       elseif ($crop==2) {$nw=1/$format;$nh=1;}
       elseif ($crop==1) {$nw=1;$nh=1/$format;}
       $nw*=$dim;$nw=($nw<1)?1:$nw;
       $nh*=$dim;$nh=($nh<1)?1:$nh;
 
       $s1=$nw/$w;
       $s2=$nh/$h;
       $h1=$nh*$s1;
       $w2=$nw*$s2;

       $s1=($h1<=$nh)?$s1:0;
       $s2=($w2<=$nw)?$s2:0;
       $scale=($s1>$s2)?$s1:$s2;
       $scale=($scale<=0)?1:$scale;

       $d=array(0,0,floor($nw),floor($nh));
       $x1=floor(($w-$d[2]/$scale)/2);$x1=$x1<0?0:$x1;
       $y1=floor(($h-$d[3]/$scale)/2);$y1=$y1<0?0:$y1;
       $s=array($x1,$y1,floor($w-2*$x1),floor($h-2*$y1));
       }

   // input/output
   switch($type1)
       {
       case 'jpg': $imgIn = ImageCreateFromJPEG; break;
       case 'png': $imgIn = ImageCreateFromPNG;  break;
       case 'gif': $imgIn = ImageCreateFromGIF;  break;
       case 'bmp': $imgIn = ImageCreateFromWBMP; break;
       default: return FALSE; break;
       }
   switch($type2)
       {
       case 'jpg': $imgOut = ImageJPEG; break;
       case 'png': $imgOut = ImagePNG;  break;
       case 'gif': $imgOut = ImageGIF;  break;
       case 'bmp': $imgOut = ImageWBMP; break;
       default: return FALSE; break;
       }

   // resample (d-destination, s-source)
   $image1 = $imgIn($photo1);
   if ($methode==0)
       {
       $image2 = ImageCreateTruecolor($d[2],$d[3]);
       ImageCopyResampled($image2, $image1,
 $d[0],$d[1], $s[0],$s[1], $d[2],$d[3], $s[2],$s[3]);
       }
   else    {
       $image2 = ImageCreate($d[2],$d[3]);
       ImageCopyResized($image2, $image1,
$d[0],$d[1], $s[0],$s[1], $d[2],$d[3], $s[2],$s[3]);
       }

   // output
   if ($type2=='jpg')
       {$imgOut($image2,$photo2,85);}
   else    {$imgOut($image2,$photo2);}
   ImageDestroy($image2);
   ImageDestroy($image1);
   }
}


function strless($t,$n){
	$text = substr($t,0,$n);
	$strlenlp = strlen($t);
	if(strlen($strlenlp>$n))
	{
		$text .= "...";
	}
	return $text;
}





$jback = 10;
$checkuser = mysql_query("SELECT * FROM sold WHERE saleid='$jback' AND userid='$id'");

if(mysql_num_rows($checkuser)!=0){
	$checkuser = mysql_fetch_assoc($checkuser);
	echo "<style type='text/css'>body{background-color:#$checkuser[used];}</style>";
}else{
	echo "<style type='text/css'>body{background-color:#EEEEEE;}</style>";
}



function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'   => $pattern
    );
} 





function notifUsers($query){
	$query = mysql_query($query);
	$row = mysql_fetch_assoc($query);
	$fromusers = NULL;
	$explode = explode('|',$row['fromid']);
	$count = $explode[count($explode) - 1];
	$explode = explode('|',$count);
	$allusersids=NULL;
	$c = 0;
	foreach($explode as $ex){
		$thisuser = mysql_query("SELECT * FROM users WHERE id='$ex'");
		$tu = mysql_fetch_assoc($thisuser);
		if($fromusers==NULL){
			$fromusers = $fromusers."<a href='userprofile.php?userid=".$tu['id']."' id='anone'>".ucfirst($tu['username'])."</a>";
		}else{
			$fromusers = $fromusers."<a href='userprofile.php?userid=".$tu['id']."' id='anone'>".ucfirst($tu['username'])."</a>,";
		}
		$c++;
	}
	$explode2 = explode('|',$row['fromid']);
	$count = $explode2[count($explode2) - 1];
	$explode2 = explode('|',$count);
	if(mysql_num_rows($query)!=0){
		$fromusers = "<a href='userprofile.php?userid=".$tu['id']."' id='anone'>".ucfirst($tu['username'])."</a> and ".$fromusers;
	}
	if($c==0){
		$fromusers = "text";
	}

	return $fromusers;
}









?>



